package com.craftinginterpreters.lox

import scala.collection.mutable

class RuntimeError(val token: Token, message: String) extends RuntimeException(message)

class Environment(encolsing: Environment) {
  def this() = this(null)

  private val values = new mutable.HashMap[String, Any]()

  def define(name: String, value: Any): Unit = {
    values(name) = value
  }

  def get(name: Token): Any = {
    if(values.contains(name.lexeme)) {
      return values.get(name.lexeme)
    }
    if(encolsing != null) {
      return encolsing.get(name)
    }
    throw new RuntimeError(name,
      "Undefined variable '" + name.lexeme + "'.")
  }
  def assign(name: Token, value: Any): Unit = {
    if(values.contains(name.lexeme)){
      values(name.lexeme) = value
      return
    }
    if(encolsing != null){
      encolsing.assign(name, value)
      return
    }
    throw new RuntimeError(name, s"Undefined variable'${name.lexeme}.'")
  }
}


class Interpreter {
  import TokenType._
  private var environment = new Environment

  private def isTruthy(obj: Any): Boolean = {
    obj match {
      case null => false
      case b: Boolean => b
      case _ => true
    }
  }

  private def isEqual(left: Any, right: Any): Boolean = {
    (left, right) match {
      case (null, null) => true
      case (null, b) => false
      case (a, b) => a.equals(b)
    }
  }

  private def checkNumberOperand(op: Token, operand: Any): Unit = {
    if (operand.isInstanceOf[Double]){
      return
    }
    throw new RuntimeError(op, "Operand must be a number.")
  }

  private def checkNumberOperands(op: Token, left: Any, right: Any): Unit = {
    (left, right) match {
      case (a: Double, b: Double) =>
        ()
      case _ =>
        throw new RuntimeError(op, "Operands must be numbers.");
    }
  }

  private def executeBlock(statements: List[Stmt], environment: Environment): Unit = {
    val previous = this.environment
    try {
      this.environment = environment
      for(statemtnt <- statements) {
        eval(statemtnt)
      }
    }finally {
      this.environment = previous
    }
  }

  def eval(stmt: Stmt): Any = {
    stmt match {
      case Expression(expression) =>
        eval(expression)
        null
      case Print(expression) =>
        val value = eval(expression)
        println(stringify(value))
        null
      case Var(name, initializer) =>
        var value: Any = null
        if(initializer != null){
          value = eval(initializer)
        }
        environment.define(name.lexeme, value)
        null
      case Block(statements) =>
        executeBlock(statements, new Environment(environment))
        null
      case If(condition, thenBranch, elseBranch) =>
        if(isTruthy(eval(condition))){
          eval(thenBranch)
        } else if(elseBranch != null){
          eval(elseBranch)
        }
        null
      case While(condition, body) =>
        while(isTruthy(eval(condition))){
          eval(body)
        }
        null
    }
  }

  def eval(expr: Expr): Any = {
    expr match {
      case Literal(value) =>
        value
      case Variable(name) =>
        environment.get(name)
      case Grouping(expr) =>
        eval(expr)
      case Unary(op, r) =>
        val right = eval(r)
        op.typ match {
          case MINUS =>
            checkNumberOperand(op, right)
            -right.asInstanceOf[Double]
          case BANG =>
            !isTruthy(right)
        }
      case Binary(l, op, r) =>
        val left = eval(l)
        val right = eval(r)
        op.typ match {
          case MINUS =>
            checkNumberOperands(op, left, right)
            left.asInstanceOf[Double] - right.asInstanceOf[Double]
          case SLASH =>
            checkNumberOperands(op, left, right)
            left.asInstanceOf[Double] / right.asInstanceOf[Double]
          case STAR =>
            checkNumberOperands(op, left, right)
            left.asInstanceOf[Double] * right.asInstanceOf[Double]
          case PLUS =>
            (left, right) match {
              case (left: Double, right: Double) =>
                return left + right
              case (left: String, right: String) =>
                return left + right
            }
            throw new RuntimeError(op,
              "Operands must be two numbers or two strings.");
          case GREATER =>
            checkNumberOperands(op, left, right)
            left.asInstanceOf[Double] > right.asInstanceOf[Double]
          case GREATER_EQUAL =>
            checkNumberOperands(op, left, right)
            left.asInstanceOf[Double] >= right.asInstanceOf[Double]
          case LESS =>
            checkNumberOperands(op, left, right)
            left.asInstanceOf[Double] < right.asInstanceOf[Double]
          case LESS_EQUAL =>
            checkNumberOperands(op, left, right)
            left.asInstanceOf[Double] <= right.asInstanceOf[Double]
          case BANG_EQUAL =>
            isEqual(left, right)
          case EQUAL_EQUAL =>
            !isEqual(left, right)
        }
      case Assign(name, value) =>
        val v = eval(value)
        environment.assign(name, v)
        v
      case Logical(left, operator, right) =>
        val l = eval(left)
        if(operator.typ == TokenType.OR){
          if(isTruthy(l)){
            return l
          }
        } else {
          if(!isTruthy(left)){
            return left
          }
        }
        eval(right)
    }
  }

  def stringify(value: Any): String = {
    value match {
      case null => "nil"
      case d: Double =>
        var text = d.toString
        if(text.endsWith(".0")){
          text = text.substring(0, text.length-2)
        }
        text
      case o =>
        o.toString
    }
  }

  def interpreter(statements: List[Stmt]): Unit = {
    try {
      for (
        stmt <- statements
      ) eval(stmt)
    } catch {
      case error: RuntimeError =>
        Lox.runtimeError(error)
    }
  }

  def interpreter(expression: Expr): Unit = {
    try {
      val value = eval(expression)
      println(stringify(value))
    }catch {
      case error: RuntimeError =>
        Lox.runtimeError(error)
    }
  }
}
