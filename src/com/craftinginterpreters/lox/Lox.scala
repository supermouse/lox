package com.craftinginterpreters.lox

import java.io.{BufferedReader, InputStreamReader}
import java.nio.charset.Charset
import java.nio.file.{Files, Paths}

object Lox {
  import TokenType._
  var hasError = false
  var hadRuntimeError = false

  def error(line: Int, message: String): Unit = {
    report(line, "", message)
  }

  def error(token: Token, message: String): Unit = {
    if(token.typ == EOF) {
      report(token.line, " at end", message)
    } else{
      report(token.line, " at '" + token.lexeme + "'", message)
    }
  }

  def runtimeError(error: RuntimeError): Unit = {
    Console.err.println(s"${error.getMessage} \n [line ${error.token.line}]")
    hadRuntimeError=true
  }

  def report(line: Int, where: String, message: String): Unit ={
    Console.err.println(s"[line $line ] Error $where : $message")
  }

  def run(source: String): Unit = {
    val scanner = new Scanner(source)
    val tokens = scanner.scanTokens()
    for(token <- tokens){
      println(token)
    }
    val parser = new Parser(tokens)
    val statements  = parser.parse()
    if (hasError) {
      return
    }
//    println(Expr.printer(expression))
    (new Interpreter).interpreter(statements)
  }

  def runFile(path:String): Unit = {
    val bytes = Files.readAllBytes(Paths.get(path))
    run(new String(bytes, Charset.defaultCharset()))
    if (hasError) {
      System.exit(65)
    }
    if(hadRuntimeError) {
      System.exit(70)
    }
  }

  def runPrompt(): Unit = {
    val input = new InputStreamReader(System.in)
    val reader = new BufferedReader(input)
    while (true){
      println("> ")
      run(reader.readLine())
      hasError = false
    }
  }

  def main(args: Array[String]): Unit = {
    if(args.length>1){
      println("Usage: jox [script]")      
    }else if(args.length == 1){
      runFile(args(0))
    } else {
      runPrompt()
    }
  }
}
