package com.craftinginterpreters.lox
import scala.collection.mutable.ListBuffer
import TokenType._

class Scanner(source: String) {
  import Scanner._
  private val tokens = ListBuffer[Token]()
  private var start = 0
  private var current = 0
  private var line = 1

  private def isAtEnd = current >= source.length

  private def advance() = {
    current+=1
    source.charAt(current-1)    
  }

  private def addToken(typ: TokenType, literal: Any = null): Unit = {
    val text = source.substring(start, current)
    tokens.addOne(Token(typ, text, literal, line))
  }

  private def Match(expected: Char): Boolean = {
    if(isAtEnd) return false
    if (source.charAt(current) != expected) return false
    current+=1
    true
  }

  private def peek(): Char = {
    if(isAtEnd) {
      '\u0000'
    }
    else source.charAt(current)
  }

  private def string(): Unit = {
    while(peek() != '"' && !isAtEnd){
      if(peek() == '\n'){
        line+=1
      }
      advance()
    }
    if(isAtEnd){
      Lox.error(line, "Unterminated string.")
    }
    advance()
    val value = source.substring(start+1, current-1)
    addToken(STRING, value)
  }

  private def peekNext(): Char =
    if(current+1 >= source.length) '\u0000'
    else source.charAt(current+1)


  private def number(): Unit = {
    while(isDigit(peek())){
      advance()
    }
    if(peek() == '.' && isDigit(peekNext())){
      advance()
      while (isDigit(peek()))
        advance()
    }
    addToken(NUMBER, source.substring(start, current).toDouble)
  }

  private def isDigit(c: Char) =
    c >= '0' && c <= '9'

  private def isAlpha(c: Char) =
    (c >= 'a' && c <= 'z') ||
    (c >= 'A' && c <= 'Z') ||
    c == '_'
  private def isAlphaNumber(c: Char) = isAlpha(c) || isDigit(c)

  private def identifier(): Unit = {
    while(isAlphaNumber(peek()))
      advance()
    val text = source.substring(start, current)
    val tye = keywords.getOrElse(text, IDENTIFIER)
    addToken(tye)
  }

  private def scanToken(): Unit = {
    val c = advance()
    c match {
      case '(' => addToken(LEFT_PAREN)
      case ')' => addToken(RIGHT_PAREN)
      case '{' => addToken(LEFT_BRACE)
      case '}' => addToken(RIGHT_BRACE)
      case ',' => addToken(COMMA)
      case '.' => addToken(DOT)
      case '-' => addToken(MINUS)
      case '+' => addToken(PLUS)
      case ';' => addToken(SEMICOLON)
      case '*' => addToken(STAR)
      case '!' =>
        val typ = if(Match('='))BANG_EQUAL else BANG
        addToken(typ)

      case '=' =>
        val typ = if(Match('='))EQUAL_EQUAL else EQUAL
        addToken(typ)

      case '<' =>
        val typ = if(Match('='))LESS_EQUAL  else LESS
        addToken(typ)

      case '>' =>
        val typ = if(Match('='))GREATER_EQUAL else GREATER
        addToken(typ)
      case '/' =>
        if(Match('/')){
          while(peek() != '\n' && !isAtEnd){
            advance()
          }
        }else{
          addToken(SLASH)
        }
      case ' ' | '\r' | '\t' =>
      case '\n' =>
        line+=1
      case '"' =>
        string()
      case c if isDigit(c) =>
        number()
      case c if isAlpha(c) => 
        identifier()
      case _ =>
        Lox.error(line, "Unexpected character.")
    }
  }

  def scanTokens(): List[Token] = {
    while(!isAtEnd){
      start = current
      scanToken()
    }
    tokens.addOne(Token(EOF, "", null, line))
    tokens.toList
  }
}

object Scanner {
  val keywords = Map(
    "and" -> AND,
    "class" -> CLASS,
    "else" -> ELSE,
    "false" -> FALSE,
    "for" -> FOR,
    "fun" -> FUN,
    "if" -> IF,
    "nil" -> NIL,
    "or" -> OR,
    "print" -> PRINT,
    "return" -> RETURN,
    "super" -> SUPER,
    "this" -> THIS,
    "true" -> TRUE,
    "var" -> VAR,
    "while" -> WHILE
  )
}