package com.craftinginterpreters.lox

trait Expr
case class Binary(left: Expr, operator: Token, right: Expr) extends Expr
case class Grouping(expression: Expr) extends Expr
case class Literal(value: Any) extends Expr
case class Unary(operator: Token, right: Expr) extends Expr
case class Variable(name: Token) extends Expr
case class Assign(name: Token, value: Expr) extends Expr
case class Logical(left: Expr, operator: Token, right: Expr) extends Expr

trait Stmt
case class Expression(expression: Expr) extends Stmt
case class Print(expression: Expr) extends Stmt
case class Var(name: Token, initializer: Expr) extends Stmt
case class Block(statements: List[Stmt]) extends Stmt
case class If(condition: Expr, thenBranch: Stmt, elseBranch: Stmt) extends Stmt
case class While(condition: Expr, body: Stmt) extends Stmt

object Expr {
  def printer(expr: Expr): String = {
    expr match {
      case Binary(l, o, r) =>
        val left = printer(l)
        val right = printer(r)
        List(o.lexeme, left, right).mkString("(", ",",")")
      case Grouping(e) =>
        val expr = printer(e)
        List("group", expr).mkString("(", ",",")")
      case Literal(v) =>
        if(v == null){
          "nil"
        }else{
          v.toString
        }
      case Unary(o, r) =>
        val right = printer(r)
        List(o.lexeme, right).mkString("(", ",",")")
    }
  }
}
