package com.craftinginterpreters.lox

import scala.collection.mutable.ListBuffer

class ParserError extends RuntimeException


class Parser(tokensList: List[Token]) {
  import TokenType._
  private val tokens = tokensList.toVector
  private var current = 0

  private def error(token: Token, message: String) = {
    Lox.error(token, message)
    new ParserError
  }

  private def previous() =
    tokens(current-1)

  private def peek() = tokens(current)

  private def isAtEnd: Boolean = peek().typ == EOF

  private def check(x: TokenType): Boolean = {
    if(isAtEnd) false
    else peek().typ == x
  }

  private def advance() = {
    if(!isAtEnd) current+=1
    previous()
  }

  def Match(types: TokenType*): Boolean = {
    @scala.annotation.tailrec
    def subMatch(types: List[TokenType]): Boolean =
      types match {
        case Nil => false
        case x::xs =>
          if(check(x)){
              advance()
            true
          } else {
            subMatch(xs)
          }
      }
    subMatch(types.toList)
  }

  def equality(): Expr = {
    var expr = comparison()
    while(Match(BANG_EQUAL, EQUAL_EQUAL)){
      val operator = previous()
      val right = comparison()
      expr = Binary(expr, operator, right)
    }
    expr
  }

  private def comparison(): Expr = {
    var expr = addition()
    while (Match(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)){
      val operator = previous()
      val right = addition()
      expr = Binary(expr, operator, right)
    }
    expr
  }

  private def consume(tpe: TokenType, message: String): Token = {
    if(check(tpe)){
      advance()
    }
    throw error(peek(), message)
  }

  private def primary(): Expr = {
    if(Match(FALSE))
      return Literal(false)
    if(Match(TRUE))
      return Literal(true)
    if(Match(NIL))
      return Literal(null)
    if(Match(NUMBER, STRING)){
      return Literal(previous().literal)
    }
    if(Match(IDENTIFIER)){
      return Variable(previous())
    }
    if(Match(LEFT_PAREN)){
      val expr = expression()
      consume(RIGHT_PAREN, "Expect ')' after expression.")
      return Grouping(expr)
    }
    throw error(peek(), "Expect expression.")
  }

  private def unary() :Expr= {
    if(Match(BANG, MINUS)){
      val operator = previous()
      val right = unary()
      Unary(operator, right)
    } else{
      primary()
    }
  }

  private def multiplication(): Expr = {
    var expr = unary()
    while(Match(SLASH, STAR)){
      val operator = previous()
      val right = unary()
      expr = Binary(expr, operator, right)
    }
    expr
  }

  private def addition(): Expr = {
    var expr = multiplication()
    while(Match(MINUS, PLUS)){
      val operator = previous()
      val right = multiplication()
      expr = Binary(expr, operator, right)
    }
    expr
  }


  private def and() = {
    var expr = equality()
    while (Match(AND)){
      val operator = previous()
      val right = equality()
      expr = Logical(expr, operator, right)
    }
    expr
  }

  private def or() = {
    var expr = and()
    while(Match(OR)){
      val operator = previous()
      val right = and()
      expr = Logical(expr, operator, right)
    }
    expr
  }

  private def assignment(): Expr = {
    val expr = or()
    if (Match(EQUAL)){
      val equals = previous()
      val value = assignment()
      expr match {
        case Variable(name) =>
            return Assign(name, value)
        case _ =>
          error(equals, "Invalid assignment target.")
      }
    }
    expr
  }

  private def expression(): Expr = {
    assignment()
  }

  private def synchronize(): Unit ={
    advance()
    while (!isAtEnd){
      if(previous().typ == SEMICOLON){
        return
      }
      peek().typ match {
        case CLASS | FUN | VAR | FOR | IF | WHILE | PRINT | RETURN =>
             return
      }
      advance()
    }
  }

  private def printStatment(): Stmt = {
    val value = expression()
    consume(SEMICOLON, "Expect ';' after value.")
    Print(value)
  }

  private def expressionStatement(): Stmt = {
    val expr = expression()
    consume(SEMICOLON, "Expect ';' after expression.")
    Expression(expr)
  }

  private def block(): List[Stmt] = {
    val statements = new ListBuffer[Stmt]
    while (!check(RIGHT_BRACE) && !isAtEnd){
      statements.addOne(declaration())
    }
    consume(RIGHT_BRACE, "Expect '}' after block.")
    statements.toList
  }

  private def ifStatement(): Stmt = {
    consume(LEFT_PAREN, "")
    val condition = expression()
    consume(RIGHT_PAREN, "Expect ')' after if condition.")
    val thenBranch = statement()
    var elseBranch: Stmt = null
    if(Match(ELSE)){
      elseBranch = statement()
    }
    If(condition, thenBranch, elseBranch)
  }

  private def whileStatemtnt(): Stmt = {
    consume(LEFT_PAREN, "Expect '(' after 'while'.")
    val condition = expression()
    consume(RIGHT_PAREN, "Expect ')' after condition.")
    val body = statement()
    While(condition, body)
  }

  private def forStatement(): Stmt = {
    consume(LEFT_PAREN, "Expect '(' after 'for'.")
    var initializer: Stmt = null
    if(Match(SEMICOLON)){
      initializer = null
    } else if (Match(VAR)){
      initializer = varDeclaration()
    }else {
      initializer = expressionStatement()
    }
    var condition: Expr = null
    if(!check(SEMICOLON)){
      condition = expression()
    }
    consume(SEMICOLON, "Expect ';' after loop condition.")

    var increment: Expr = null
    if(!check(RIGHT_PAREN)){
      increment = expression()
    }
    consume(RIGHT_PAREN, "Expect ')' after for clauses.")
    var body = statement()
    if (increment != null){
      body = Block(List(body, Expression(increment)))
    }
    if (condition == null){
      condition = Literal(true)
    }
    body = While(condition, body)
    if(initializer != null){
      body = Block(List(initializer, body))
    }
    body
  }

  private def statement(): Stmt = {
    if(Match(PRINT)){
      printStatment()
    } else if(Match(LEFT_BRACE)){
      Block(block())
    } else if (Match(IF)){
      ifStatement()
    } else if(Match(FOR)){
      forStatement()
    } else if(Match(WHILE)){
      whileStatemtnt()
    } else {
      expressionStatement()
    }
  }

  private def varDeclaration(): Stmt = {
    val name = consume(IDENTIFIER, "Expect variable name.")
    var initializer: Expr = null
    if(Match(EQUAL)){
      initializer = expression()
    }
    consume(SEMICOLON, "Expect ';' after variable declaration.")
    Var(name, initializer)
  }

  private def declaration(): Stmt = {
    try{
      if(Match(VAR))
        varDeclaration()
      else
        statement()
    } catch {
      case error: ParserError =>
        synchronize()
        null
    }
  }

  def parse(): List[Stmt] = {
    val statements = new ListBuffer[Stmt]
    while (!isAtEnd){
      val stmt = statement()
      statements.addOne(stmt)
    }   
    statements.toList
  }

//  def parse(): Expr ={
//    try {
//      expression()
//    } catch{
//      case e: ParserError => null
//    }
//  }
}
